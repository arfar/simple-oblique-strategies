Simple Oblique Strategies
=========================

This is my really super simple HTML+CSS+JS implementation of Oblique Strategies.
There really isn't much to it - but it works nicely for what I need and is
handily in a single file for everything.

Might think about adding a title bar icon thingy at some point too.

If you want to read more about the concept, find it on Wikipedia here: 
https://en.wikipedia.org/wiki/Oblique_Strategies

As far as I can claim copyright on this code and content of this repo
(which I certainly do not over the Oblique Strategies themselves -
if copyright were to subsist in them at all), this is licenced under
AGPLv3.